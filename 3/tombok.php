<?php
//tömbök (arrays)

$colors = ['red','green','blue'];// oldschool: array('red','green','blue')

//echo $colors;//warning mert a tömb nem alakítható kiírható stringgé

var_dump($colors);
/*print_r($colors);*/

//szép kiírás fejlesztés alatt nem primitív elemekre

echo '<pre>'.var_export($colors, true).'</pre>';

//a tömb egy eleme ha primitív, az kiírható
echo $colors[1];//tömb 1 elemének elérése meghatározott indexről
