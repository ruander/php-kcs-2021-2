<?php
//'dobjunk egy kockával' (1-6)
$dice = rand(1,6);//0-int_rand_max

var_dump($dice);
echo '<h2>A dobás eredménye: '.$dice.'</h2>';


//írjuk ki, hogy a dobott érték páros-e vagy páratlan
/*
Elágazás
if( feltétel [true|false] ){
    //kódblokk - igaz esetben
}else{
    //kódblokk - hamis esetben
}
 */
$answer = '';//ebben tároljuk majd a választ hogy páros vagy páratlan a mit az ágakban felülírunk az aktuális stringgel
if( $dice%2 === 0 ){// operátor: == -> loose comparison - csak értékegyezés vizsgálat;  === -> strict comparison - érték ÉS tipusegyezés vizsgálat
    //igaz ág - páros
    $answer = 'páros';
}else{
    //hamis ág
    $answer = 'páratlan';//páratlan
}

echo "Ami $answer.";


//switch - case
/*
switch(érték){

    case 1:
            ...kódblokk
        break;
    case 'helo':
            ...kódblokk
        break;

    default:
        ..kódblokk
        break;
}
...
 */
$action = 'update';//művelethalmaz (Create, Read,  Update, Delete) - CRUD

switch($action){

    case 'delete':
        echo '<br>törlés';
        break;

    case 'update':
        echo '<br>módosítás';
        break;

    case 'create':
        echo '<br>létrehozás';
        break;

    default:
        echo '<br>olvasás';
        break;
}
