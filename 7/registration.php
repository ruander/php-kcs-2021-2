<?php
//külső forrás betöltése mintha ide lenne gépelve
//include "functions.php";//ha nincs meg a file, warning és tovább
require "functions.php";//ha nincs meg a file, fatal error
include_once "functions.php";// _once csak akkor tölti be ha még nem volt betöltve
require_once "functions.php";
//ha van mit feldolgozni akkor feldolgozzuk
if (!empty($_POST)) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];// itt tároljuk a hibákat ha vannak
    //Név min 3 karakter
    $name = filter_input(INPUT_POST, "name");
    //takarítsuk le a felesleges space-eket (és egyebet)
    $name = trim($name);//ltrim() elejéről ,rtrim() végéről
    //var_dump(mb_strlen($name, "utf-8"));//var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $errors['name'] = '<span class="error">Nem érvényes formátum! (minimum 3 karakter)</span>';
    }
    //email, legyen emailnek látszó string
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $errors["email"] = '<span class="error">Nem érvényes formátum!</span>';
    }

    //jelszó 1 min 6 karakter
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password) < 6) {
        $errors["password"] = '<span class="error">Nem érvényes formátum! (minimum 6 karakter)</span>';
    } elseif ($password !== $repassword) {
        //jelszó 2 egyeznie kell a jelszó 1-el
        $errors["repassword"] = '<span class="error">A jelszavak nem egyeztek!</span>';
    }else{
        //jók a jelszavak
        /*$secret_key ='S3cR3T_k3Y!';
        echo $password = md5($password.$secret_key);*/
        $password = password_hash($password, PASSWORD_BCRYPT);
    }
    //Elfogadás checkbox legyen kipipálva
    if (!filter_input(INPUT_POST, 'terms')) {
        $errors["terms"] = '<span class="error">Kötelező kipipálni!</span>';
    }


    if (empty($errors)) {
        //nincs hiba az űrlapon, tegyünk rendet az adataink között
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        //mikor történt a regisztráció (time_created), tegyük a tömbünkbe az adatokhoz
        $data['time_created'] = date('Y-m-d H:i:s');


        echo '<pre>' . var_export($data, true) . '</pre>';

    }

}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
            margin: 10px;
        }

        label.inline {
            display: block;
        }

        .error {
            font-size: .8em;
            font-style: italic;
            color: #f00;
        }
    </style>
</head>
<body>
<section class="registration">
    <form method="post">
        <!--Név-->
        <label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Gipsz Jakab" value="<?php echo getValue('name'); ?>">
            <?php
            //ha van hiba a mezőn akkor kiírjuk
            echo getError('name');
            ?>
        </label>
        <!--Email-->
        <label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo getValue('email'); ?>">
            <?php
            //ha van hiba a mezőn akkor kiírjuk
            echo getError('email');
            ?>
        </label>
        <!--Jelszó 1 min 6 karakter-->
        <label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="password" placeholder="******" value="">
            <?php
            //ha van hiba a mezőn akkor kiírjuk
            echo getError('password');
            ?>
        </label>
        <!--Jelszó 2 egyezés-->
        <label>
            <span>Jelszó mégegyszer<sup>*</sup></span>
            <input type="password" name="repassword" placeholder="******" value="">
            <?php
            //ha van hiba a mezőn akkor kiírjuk
            echo getError('repassword');
            ?>
        </label>
        <label class="inline">
            <input type="checkbox" name="terms" value="1"
                <?php
                /*if(filter_input(INPUT_POST,'terms')){
                    echo 'checked';
                }*/
                //short hand, shorten, rövid if
                // (condition) ? true : false;
                echo getValue('terms') ? 'checked' : '';
                ?>>
            <?php
            //ha van hiba a mezőn akkor kiírjuk
            echo getError('terms');
            ?>
            Elolvastam és megértettem az adatkezelési tájékoztatót.

        </label>
        <!--<input type="submit" value="Mehet">-->
        <button>Regisztrálok</button>
    </form>
</section>
</body>
</html>
