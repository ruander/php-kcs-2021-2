<?php
/**
 * szöveges tipusu input elemek beírt value értékét adja vissza
 * @param $fieldName  | mező neve
 * @return mixed
 */
function getValue($fieldName){
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * Input elemek hibaüzenetét adja vissza egy $errors[fieldName] szerkezetű hibatömbből
 * @param $fieldName
 * @return false|mixed
 */
function getError($fieldName){
    global $errors;//az eljárás idejére globálissá tesszük az errors tömböt

    if( isset($errors[$fieldName]) ){
        return $errors[$fieldName];
    }

    return false;
}
