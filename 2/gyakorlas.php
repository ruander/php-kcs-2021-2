<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gyakorlás</title>
</head>
<body>
<?php
//1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.

$a = 25;
$surface = $a**2*6;
echo "<div>Egy {$a}m élhosszúságú kocka felülete: {$surface}m<sup>2</sup></div>";

/**
 * @todo HF: gyakorló feladatok 1-13 ig
 *
 */
//4.
echo "<div>4.</div>";
$vat = 15;//%
$product_price = 200;//115% netto*1.15
$product_net_price= round($product_price/((100+$vat)/100));// x = brutto/((100+15)/100)
$product_vat = $product_price - $product_net_price;
$currency = 'HUF';//pénznem
/**
 * kerekítés:
fel: ceil()
matematikai .5 -fel - le: round()
le: floor()
 *
 *
 */

echo "Netto: $product_net_price.- $currency<br>
      Áfa: $product_vat.- $currency<br>
      Ár: $product_price.- $currency";

//9.Írjon egy php programot, amellyel a Google szóra egy linket állít elő és a link a www.google.com oldalra mutat.
echo '<br><a href="https://www.google.com" target="_blank">google oldala</a>';

?>
</body>
</html>
