<?php

//egysoros megjegyzés

/*
több
soros
megjegyzés
 */

#ez is megjegyzés

/**
 * Speciális megjegyzés PHPDoc
 * @todo Valami tennivaló a programrésszel
 */

echo "<h1 style=\"color:blue;\">Második alkalom</h1>";//operátor: \ -> escape -> az utána következő karaktert kilépteti a nyelvi végrehajtásból
echo '<h2 style="color:red;">Változók</h2>';

//Skaláris értékek (primitív tipusok)

/*
változónév értékadó operátor érték

$user_id = 5; //snake case
$userId = 5; //camel case
$user_1_id = 10;
 */

$dice = 3; //tipus: egész szám | integer vagy int, 0,1,2,...,-5
$isActive = true;//tipus: logikai | boolean vagy bool, true|false
$price = 49.95; //tipus: lebegő pontos szám | floating point number vagy float
$userName = "Horváth György";//tipus: szöveg | string

echo $dice;
echo "<br>Ár:$price";
echo '<br>Név:' . $userName;// operátor: . -> konkatenáció | összefűzi stringként a jobb és bal oldalán található értékeket (stringgé is alakítja
print "<br>aktív? $isActive";

//műveletek
//string (konkatenáció)
$firstName = 'George';
$lastName = 'Horváth';

$userName = $firstName . ' ' . $lastName;//felülírjuk a $userName változót (override/redeclare)
echo '<br>Név:' . $userName;

/*$g = 9.81;

echo '<pre>';//fejlesztés közben
var_dump($g);
$g = 1;
var_dump($g,$lastName);*/
//állandók (NAGYBETŰ, SNAKE_CASE)

//define('TEST', 50);

const _G = 9.81;

//_G = 1; állandó nem írható felül
echo '<pre>';
var_dump(_G);

var_dump(PHP_VERSION);
//const PHP_VERSION = 8; beépített állandó nem módosítható)
var_dump(PHP_VERSION);
var_dump(__LINE__);
var_dump(__FILE__);

echo '<br>', $firstName , ' ', $lastName, '<br>';

//a és b befogóval rendelkező derékszögű háromszög átfogójának mérete
/**
 * @link https://www.php.net/manual/en/ref.math.php
 */
$a = 3;
$b = 4;

//c^2=a^2+b^2
//c = négyzetgyök(a^2+b^2);
//$c = sqrt($a*$a + $b*$b);
$c = sqrt($a**2 + $b**2);
//var_dump($c);
//kiírás

echo "Egy {$a}m és {$b}m befogójú háromszög átfogója {$c}m";
