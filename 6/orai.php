<?php

//a GET tipusu adatok (URL paraméter átadás)
var_dump($_GET);//GET szuperglobális tömb

//A filter_input
$a = filter_input(INPUT_GET,'a');//$a = $_GET['a']; <-- így NE!
echo "<br>az a értéke : $a <br>";

//a POST tipusu adatok űrlapból érkeznek alapvetően
var_dump($_POST);//POST szuperglobális tömb

echo "<br>";

//a REQUEST tipusu adatok
var_dump($_REQUEST);//REQUEST szuperglobális tömb

echo "<br>";
