<?php

//ha nem üres a post, akkor dolgozzuk fel...
if( !empty($_POST)  ){

    //var_dump($_POST);
    //hibakezelés
    $errors = [];//hibák halmaza
    //szűrjük le az adat nevű mezőt hogy csak azokat az értékeket fogadjuk el amik egész számok
    $N = filter_input(INPUT_POST,'N',FILTER_VALIDATE_INT);
    //var_dump($data);
    if($N < 1 ){
        $errors['N'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //...


    if(empty($errors)){
        ////ha nincs hiba, akkor minden adat OK, jöhet a művelet
        /// //Feladatgyüjtmény 8-9-10 egy feladat8.php fileban
        //írjunk ki annyi 'A' betűt ahányas értéket kaptunk
        //8as feladat
        echo str_repeat('XO',$N);
        echo '<br><br>';

        //9es feladat kiegészítés
        for($i=1;$i<=$N;$i++){
            echo 'OX';
        }
        echo '<br><br>';

        //10. feladat kiegészítése
        for($j=1;$j<=$N;$j++){

            //8as feladat
            echo str_repeat('XO',$N);
            echo '<br>';

            //9es feladat kiegészítés
            for($i=1;$i<=$N;$i++){
                echo 'OX';
            }
            echo '<br>';

        }


    }
}

//@todo HF: feladatgyujtemény-2.pdf    12-13, 14-15, 17, 20 feladatok


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Feladatgyüjtemény 8. - 10. feladat</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        form {
            width:90%;
            margin: 50px auto;
            max-width: 600px;
        }
        label {
            display:flex;
            flex-flow: column nowrap;
            margin: 15px;
        }
        .error {
            font-size:.7em;
            color:#f00;
            font-style: italic;
        }

    </style>
</head>
<body>
<form method="post">
    <label>
        N szám (pozitív egész szám)
        <input name="N" type="text" placeholder="14" value="<?php echo filter_input(INPUT_POST,'N'); ?>">
        <?php
        //mezőhiba kiírása, ha van
        if( isset($errors['N']) ){
            echo  $errors['N'];
        }

        ?>
    </label>
    <button>Mehet</button>
</form>
</body>
</html>
