<?php

//ha nem üres a post, akkor dolgozzuk fel...
if( !empty($_POST)  ){

    //var_dump($_POST);
    //hibakezelés
    $errors = [];//hibák halmaza
    //szűrjük le az adat nevű mezőt hogy csak azokat az értékeket fogadjuk el amik egész számok
    $data = filter_input(INPUT_POST,'adat',FILTER_VALIDATE_INT);
    //var_dump($data);
    if($data < 1 ){
        $errors['adat'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //...


    if(empty($errors)){
        ////ha nincs hiba, akkor minden adat OK, jöhet a művelet
        //írjunk ki annyi 'A' betűt ahányas értéket kaptunk

        for($i=1;$i<=$data;$i++){
            echo '<br>A';
        }
        echo str_repeat('A',$data);

    }
}



?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap 2 - self processing</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        form {
            width:90%;
            margin: 50px auto;
            max-width: 600px;
        }
        label {
            display:flex;
            flex-flow: column nowrap;
            margin: 15px;
        }
        .error {
            font-size:.7em;
            color:#f00;
            font-style: italic;
        }

    </style>
</head>
<body>
<form method="post">
    <label>
        Adat (pozitív egész szám)
        <input name="adat" type="text" placeholder="14" value="<?php echo filter_input(INPUT_POST,'adat'); ?>">
        <?php
        //mezőhiba kiírása, ha van
        if( isset($errors['adat']) ){
            echo  $errors['adat'];
        }

        ?>
    </label>
    <button>Mehet</button>
</form>
</body>
</html>
