-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Nov 09. 18:25
-- Kiszolgáló verziója: 10.4.21-MariaDB
-- PHP verzió: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `php_tanfolyam_kcs2`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(64) NOT NULL,
  `spass` varchar(64) NOT NULL,
  `stime` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `sessions`
--

INSERT INTO `sessions` (`sid`, `spass`, `stime`) VALUES
('gt5aee5lqnbsk7p3uf584nib15', '00fa36d61a14cbf05474c91c5b4d4cc9', 1636478533);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `time_created`, `time_updated`) VALUES
(1, 'superadmin', 'hgy@iworkshop.hu', '$2y$10$kcSivyhjGvGEGYlXJ/yfSuJsPJJr218JMTGZPKJ7qf1/hAw4AIC1u', 1, '2021-10-28 17:49:12', '2021-10-18 18:02:00'),
(3, 'Teszt User', 't@t.t', '$2y$10$e4VrzikCXffYGq2GqY/1neaGqXeKDCrM.DrB6stqWSvjFqUz3kPGS', 1, '2021-10-28 19:04:56', NULL),
(8, 'ujabb teszt', 'u@u.u', '$2y$10$/DIhemCBkWOC3MUX.wa1be1znkx6a3YmjtRzOe3zWrLAKbDRJoOb.', 1, '2021-10-28 19:18:14', NULL),
(10, 'Teszt X', 'x@x.x', '$2y$10$bXvxy4V2fSyQf4rgNKdL/.FVaGGaYq74R5.Tv4OcY2t639099IN6S', 1, '2021-10-28 19:21:21', NULL),
(11, 'teszt users', 'g@g.g', '$2y$10$d6/PUoYFz4rz.OOaPyXhdemz9UPvQmEZFL9.Gaqrl1Zea4WCJlQhu', 1, '2021-10-28 20:00:52', NULL),
(12, 'ttt', 't@t.ttt', '$2y$10$fXdZ27DbUawe21IPmkwU7uPejEAo/Omun0FQtDV49efhuUd21x4M.', 0, '2021-11-02 17:32:07', NULL),
(13, 'bbbb', 'b@bbbb.bb', '$2y$10$xz2E76vI1XmbVCX4ujKAAeopRxfHLmVpnEYLzyD0u.XssZ3mIkCC.', 1, '2021-11-02 17:32:31', '2021-11-02 20:02:53'),
(16, 'keddi teszt új', 'a@b.c', '$2y$10$Ap9MxPAWBOICNQOnaSWMzOrV/EXy8DOG.9OwMLQdF/XqvOizHHiiK', 1, '2021-11-02 20:07:22', '2021-11-02 20:11:10'),
(17, 'teszt új', 'z@z.z', '$2y$10$5Kf65eOudQJFErUIo5EgteWeM7sySGkiKaTbo./MFMEnPxFqJ58e2', 0, '2021-11-04 17:21:26', '2021-11-04 17:29:26');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
