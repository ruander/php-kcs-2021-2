<?php
require "../config/connect.php";/** @var $link mysqli */
require "../config/functions.php";//saját eljárások
require "../config/settings.php";//rendszer beállítások
//munkafolyamat indítása
//https://www.php.net/manual/en/book.session.php
session_start();
//print_r($_SESSION);
$infoBox = '<div class="info">
Irja be a bejelentkezési adatokat!
        <!--Hibás email/jelszó páros!-->
    </div>';

if (!empty($_POST)) {
    if (login()) {
        //minden ok, mehetünk az indexre
        header('location:index.php');
        exit();
    } else {
        $infoBox = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>
                  Hibás Email/Jelszó páros!
                </div>';
    }
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Bejelentkezés az admin felületre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo APP_ADMIN_URL; ?>"><b>Ruander</b> Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Bejelentkezés</p>
            <?php echo $infoBox; ?>
            <form method="post">
                <label class="input-group mb-3">
                    <input type="text" name="email" class="form-control" placeholder="email@cim.hu" value="<?php echo getValue('email'); ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </label>
                <label class="input-group mb-3">
                    <input type="password" class="form-control"  name="password" placeholder="******" value="">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </label>
                <div class="row">
                    <div class="col-6">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Emlékezz rám
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary btn-block">Bejelentkezés</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
</body>
</html>
