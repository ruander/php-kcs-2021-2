<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo APP_ADMIN_URL; ?>" class="brand-link">
        <img src="https://via.placeholder.com/150x55" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-2 pb-2 mb-2 d-flex align-items-center">
            <div class="image">
                <img src="https://picsum.photos/115" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block "><?php echo $_SESSION['userdata']['name']; ?></a>
                <div class="logout">

                    <a href="?logout"><i class="fas fa-sign-out-alt"></i> Kilépés</a>
                </div>
            </div>
        </div>

        <!-- SidebarSearch Form
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>-->

        <!-- Sidebar Menu -->
        <?php
        //menu elkészítése a tömből
        $menuHtml = '<nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">MENÜ</li>';
        /** @var $adminMenu */
        foreach ($adminMenu as $menuId => $menuItem) {
            $menuHtml .= '<li class="nav-item">
                            <a href="?p=' . $menuId . '" class="nav-link">
                              <i class="nav-icon ' . $menuItem['icon'] . '"></i>
                              <p>' . $menuItem['title'] . '</p>
                            </a>
                          </li>';
        }
        $menuHtml .= '</ul></nav>';

        echo $menuHtml;
        ?>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
