<?php
/**
 * @todo HF2 űrlap form elemek legyenek beillesztve a sablonba (html+css)
 */
//Users
/** @var $baseURL string */
//önálló futtatás elleni védelem
if(!isset($link)){
    header('location:index.php');
    exit();
}
/** @var $link mysqli */
//lokális erőforrások
$dbTable = 'users';
$action = filter_input(INPUT_GET, 'action') ?: 'list';
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?: null;

//ha van mit feldolgozni akkor feldolgozzuk
if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];// itt tároljuk a hibákat ha vannak
    //Név min 3 karakter
    $name = strip_tags(trim(filter_input(INPUT_POST, "name")));

    if (mb_strlen($name, "utf-8") < 3) {
        $errors['name'] = '<span class="error">Nem érvényes formátum! (minimum 3 karakter)</span>';
    }

    //email, legyen emailnek látszó string
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $errors["email"] = '<span class="error">Nem érvényes formátum!</span>';
    } else {//formátumra jó, foglalt-e?
        $result = mysqli_query($link, "SELECT id FROM $dbTable WHERE email = '$email' LIMIT 1") or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //ha van ilyen akkor már foglalt
        if (!empty($row) && $row[0] != $id) { //ha van már ilyen és az nem a módosítani kívánt azonosítóhoz tartozik, akkor van hiba
            $errors["email"] = '<span class="error">Már foglalt email cím!</span>';
        }
    }

    //jelszó 1 min 6 karakter

    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    if ($action == 'create' || $password != '') {//jelszó ellenőrzés új esetben mindenképp, módosításkor csak ha az 1es mezőbe van legalább 1 karakter
        if (mb_strlen($password) < 6) {
            $errors["password"] = '<span class="error">Nem érvényes formátum! (minimum 6 karakter)</span>';
        } elseif ($password !== $repassword) {
            //jelszó 2 egyeznie kell a jelszó 1-el
            $errors["repassword"] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            //jelszó kódolása tároláshoz
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }

    //státusz legyen most fix 1
    $status = filter_input(INPUT_POST, 'status') ?: 0;

    if (empty($errors)) {
        //nincs hiba az űrlapon, tegyünk rendet az adataink között
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];

        //tároljuk el adabázisban
        if ($action == 'create') {//új felvitel
            //mikor történt a regisztráció (time_created), tegyük a tömbünkbe az adatokhoz
            $data['time_created'] = date('Y-m-d H:i:s');

            $qry = "INSERT INTO 
            $dbTable (
                   name, 
                   email, 
                   password, 
                   status, 
                   time_created
                   ) 
            VALUES (
                    '{$data['name']}', 
                    '{$data['email']}', 
                    '{$data['password']}', 
                    '{$data['status']}', 
                    '{$data['time_created']}'
                    )";
        } else {//módosítás
            $data['time_updated'] = date('Y-m-d H:i:s');
            //jelszó felülírás ha kell
            $passUpdate = $password != '' ? "password = '{$data['password']}'," : "";
            $qry = "UPDATE $dbTable 
                    SET
                        name = '{$data['name']}',
                        email ='{$data['email']}',
                        status = '{$data['status']}',
                        $passUpdate
                        time_updated = '{$data['time_updated']}'
                    WHERE id = '$id'
            LIMIT 1";
        }
        //felvitel vagy update futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //echo '<pre>' . var_export($_SERVER, true) . '</pre>';
        //file átirányítása az üres form-ra (saját magára)
        header('location:' . $baseURL);
        exit();
    }

}


switch ($action) {//output kialakítása
    case 'delete':
        if ($id) {//ha kaptunk id-t töröljük a rekordot ami, ahhoz tartozik
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = '$id' LIMIT 1") or die(mysqli_error($link));
        }
        //átirányítás a listára
        header('location:' . $baseURL);
        exit;
        break;

    case 'edit':
        //ha van id, kérjük le az ahhoz tartozó adatokat az adatbázisból
        if ($id) {
            $qry = "SELECT name,email,status FROM $dbTable WHERE id = '$id' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $rowUser = mysqli_fetch_assoc($result);
        }
        //var_dump($rowUser);
        $formTitle = '<h2>Módosítás - ['.$id.' - '.$rowUser['name'].']</h2>';
        //break;//nincs break hogy a create ágban található űrlapot ne keljen ismételni.
    case 'create':
        $rowUser = $rowUser ?? [];//ha nincs lekérés(create ág) akkor legyen üres tömb
        $formTitle ??= '<h2>Új felvitel</h2>';
        //űrlap összeállítása
        $form = $formTitle;
        $form .= '<a href="'.$baseURL.'">&longleftarrow; vissza</a>';
        $form .='<form method="post">';
                    //Név
        $form .= '<label>
                <span>Név<sup>*</sup></span>
                <input type="text" name="name" placeholder="Gipsz Jakab" value="' . getValue('name', rowData: $rowUser) . '">';

        $form .= getError('name');//hiba ha van, belefűzzük
        $form .= '</label>';
//PURE PHP űrlap elemek
//email
        $form .= '<label>
                <span>Email<sup>*</sup></span>
                <input type="text" name="email" placeholder="email@cim.hu" value="' . getValue('email', rowData: $rowUser) . '">';
        $form .= getError('email');
        $form .= '</label>';
//jelszó1
        $form .= '<label>
                <span>Jelszó<sup>*</sup></span>
                <input type="password" name="password" placeholder="******" value="">';
        $form .= getError('password');
        $form .= '</label>';

//jelszó2
        $form .= '<label>
                <span>Jelszó mégegyszer<sup>*</sup></span>
                <input type="password" name="repassword" placeholder="******" value="">';
        $form .= getError('repassword');
        $form .= '</label>';
//terms
        $form .= '<label class="inline">
                <input type="checkbox" name="status" value="1"' . (getValue('status', rowData: $rowUser) ? 'checked' : '') . '> aktív?';
        $form .= '</label>';

//küldés gomb és form zárás
        $form .= '<button>'.($action === 'create' ? 'Új felvitel' : 'Módosítás').'</button></form>';
        //
        $output = $form;
        break;

    default://read
        //adattábla lekrése
        $qry = "SELECT id,name,email,status FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        $table = ' <a class="btn btn-primary mb-3" href="'.$baseURL.'&amp;action=create">Új felvitel</a>
           <table class="table table-striped table-hover">';//tábla nyitása
        $table .= '<tr>
              <th>ID</th>
              <th>név</th>
              <th>email</th>
              <th>státusz</th>
              <th>művelet</th>
            </tr>';

//sorok ciklusból
        while (($row = mysqli_fetch_assoc($result)) !== null) {
            $table .= '<tr>
                  <td>' . $row['id'] . '</td>
                  <td>' . $row['name'] . '</td>
                  <td>' . $row['email'] . '</td>
                  <td>' . $row['status'] . '</td>
                  <td> <a class="btn btn-sm btn-warning text-white"href="'.$baseURL.'&amp;action=edit&amp;id=' . $row["id"] . '"><i class="fas fa-pencil-alt"></i></a>   <a href="'.$baseURL.'&amp;action=delete&amp;id=' . $row["id"] . '"  onclick="return confirm(\'Biztos törölni akarod?\')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a> </td>
              </tr>';
        }


        $table .= '</table>';
        //
        $output = $table;

        break;
}

//switchben kialakított tartalom kiírása
//echo $output;//mivel modul, ezért majd az index irja ki
