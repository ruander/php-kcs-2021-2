<?php
/**
 * @todo HF2 űrlap form elemek legyenek beillesztve a sablonba (html+css)
 */
//Users
/** @var $baseURL string */
//önálló futtatás elleni védelem
if(!isset($link)){
    header('location:index.php');
    exit();
}
/** @var $link mysqli */
//lokális erőforrások
$dbTable = 'articles';
$action = filter_input(INPUT_GET, 'action') ?: 'list';
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?: null;

//ha van mit feldolgozni akkor feldolgozzuk
if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];// itt tároljuk a hibákat ha vannak
    //Név min 3 karakter
    $title = mysqli_real_escape_string($link, strip_tags(trim(filter_input(INPUT_POST, "title"))));

    if (mb_strlen($title, "utf-8") < 1) {
        $errors['title'] = '<span class="error">Kötelező címet adni!</span>';
    }
    //seo_title (readonly)
    //lead
    $lead =  mysqli_real_escape_string($link, strip_tags(trim(filter_input(INPUT_POST, "lead"))));
    if (mb_strlen($lead, "utf-8") > 500) {
        $errors['lead'] = '<span class="error">Max 500 karakter!</span>';
    }

    //content
    //image (url)
    //author
    //time_published (Y-m-d H:i:s)


    //státusz legyen most fix 1
    $status = filter_input(INPUT_POST, 'status') ?: 0;

    if (empty($errors)) {
        //nincs hiba az űrlapon, tegyünk rendet az adataink között
        $data = [
            'title' => $title,
            'lead' => $lead,
            'status' => $status
        ];
        //var_dump($data);
        //tároljuk el adabázisban
        if ($action == 'create') {//új felvitel
            //mikor történt a regisztráció (time_created), tegyük a tömbünkbe az adatokhoz
            $data['time_created'] = date('Y-m-d H:i:s');

            $qry = insertSQLQuery($dbTable,$data);//insert query segédeljárás


        } else {//módosítás
            $data['time_updated'] = date('Y-m-d H:i:s');

            $qry = updateSQLQuery($dbTable,$id,$data);

        }

        //felvitel vagy update futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //echo '<pre>' . var_export($_SERVER, true) . '</pre>';
        //file átirányítása az üres form-ra (saját magára)
        header('location:' . $baseURL);
        exit();
    }
}

switch ($action) {//output kialakítása
    case 'delete':
        if ($id) {//ha kaptunk id-t töröljük a rekordot ami, ahhoz tartozik
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = '$id' LIMIT 1") or die(mysqli_error($link));
        }
        //átirányítás a listára
        header('location:' . $baseURL);
        exit;
        //break;

    /** @noinspection PhpMissingBreakStatementInspection */
    case 'edit':
        $rowData = [];
        //ha van id, kérjük le az ahhoz tartozó adatokat az adatbázisból
        if ($id) {
            $qry = "SELECT title,seo_title,lead,status FROM $dbTable WHERE id = '$id' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $rowData = mysqli_fetch_assoc($result);
        }
        //var_dump($rowData);
        $formTitle = '<h2>Módosítás - ['.$id.' - '.$rowData['title'].']</h2>';
    //break;//nincs break hogy a create ágban található űrlapot ne keljen ismételni.
    case 'create':
        $rowData = $rowData ?? [];//ha nincs lekérés(create ág) akkor legyen üres tömb
        $formTitle ??= '<h2>Új felvitel</h2>';
        //űrlap összeállítása
        $form = $formTitle;
        $form .= '<a href="'.$baseURL.'">&longleftarrow; vissza</a>';
        $form .='<form method="post">';
        //Cikk
        $form .= '<label>
                <span>Cím<sup>*</sup></span>
                <input type="text" name="title" placeholder="Cikk címe" value="' . getValue('title', rowData: $rowData) . '">';

        $form .= getError('title');//hiba ha van, belefűzzük
        $form .= '</label>';
//PURE PHP űrlap elemek
        //seo_title (readonly)

        //lead
        $form .= '<label>
                <span>Bevezető (max 500 karakter)</span>';
        $form .= getError('title');//hiba ha van, belefűzzük
        $form .= '<textarea name="lead" placeholder="bevezető...">' . getValue('lead', rowData: $rowData) . '</textarea>';
        /** @noinspection DuplicatedCode */
        $form .= '</label>';

        //content
        //image (url)
        //author (alapból legyen value benne (aki be van lépve user neve) - de csak ÚJ ESETBEN töltsük ki ha üres a mező)
        //time_published (Y-m-d H:i:s)
//status
        $form .= '<label class="inline">
                <input type="checkbox" name="status" value="1"' . (getValue('status', rowData: $rowData) ? 'checked' : '') . '> aktív?';
        $form .= '</label>';

//küldés gomb és form zárás
        $form .= '<button>'.($action === 'create' ? 'Új felvitel' : 'Módosítás').'</button></form>';
        //
        $output = $form;
        break;

    default://read
        //adattábla lekrése
        $qry = "SELECT id,title,author,status FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        $table = ' <a class="btn btn-primary mb-3" href="'.$baseURL.'&amp;action=create">Új felvitel</a>
           <table class="table table-striped table-hover">';//tábla nyitása
        $table .= '<tr>
              <th>ID</th>
              <th>Cím</th>
              <th>Szerző</th>
              <th>státusz</th>
              <th>művelet</th>
            </tr>';

//sorok ciklusból
        while (($row = mysqli_fetch_assoc($result)) !== null) {
            $table .= '<tr>
                  <td>' . $row['id'] . '</td>
                  <td>' . $row['title'] . '</td>
                  <td>' . $row['author'] . '</td>
                  <td>' . $row['status'] . '</td>
                  <td> <a class="btn btn-sm btn-warning text-white" href="'.$baseURL.'&amp;action=edit&amp;id=' . $row["id"] . '"><i class="fas fa-pencil-alt"></i></a>   <a href="'.$baseURL.'&amp;action=delete&amp;id=' . $row["id"] . '"  onclick="return confirm(\'Biztos törölni akarod?\')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a> </td>
              </tr>';
        }


        $table .= '</table>';
        //
        $output = $table;

        break;
}

//switchben kialakított tartalom kiírása
//echo $output;//mivel modul, ezért majd az index irja ki
