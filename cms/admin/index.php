<?php
require "../config/connect.php";/** @var $link mysqli */
require "../config/functions.php";//saját eljárások
require "../config/settings.php";//rendszer beállítások
//munkafolyamat indítása
session_start();
//print_r($_SESSION);
//melyik oldalon járunk, ha nem tudjuk 0 azaz dashboard
$page = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;
//baseURL kialakítása
$baseURL = APP_ADMIN_URL.'index.php?p='.$page;

if (filter_input(INPUT_GET, 'logout') !== null) {//kilépés ha kell
    logout();
}
$auth = auth();//ellenőrzés
if (!$auth) {//ha nem jó az ellenőrzés
    header('location:login.php');
    exit();
}

//modul kezelés
/** @var $adminMenu array a settingsből */
$modulFile = MODULE_DIR.$adminMenu[$page]['modulName'].MODULE_EXT;//module file név kialakítása
//$output //valamelyik ágból lesz vagy modul vagy itt
//ha létezik töltsük be
if(file_exists($modulFile)){//itt keletkezik output
    include $modulFile;
}else{
    $output = 'Nincs ilyen modul: '.$modulFile;
}

//ha nincs érvényes bejelentkezés akkor irány a login
$userBar = '<section class="userbar">
                <span>Üdvözlet kedves <b>' . $_SESSION['userdata']['name'] . '</b>!</span>
                |
                <a href="?logout=true">kijelentkezés</a>
            </section>';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ruander PHP tanfolyam - Admin felület</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <?php
    include "includes/topbar.inc";
    include "includes/sidebar.inc";

    ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Blank Page</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Blank Page</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo $output; ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Footer
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    include "includes/footer.inc";
    ?>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
</body>
</html>
