<?php
require "../config/connect.php";/** @var $link mysqli */
require "../config/functions.php";//saját eljárások
require "../config/settings.php";//rendszer beállítások
//munkafolyamat indítása
session_start();
//print_r($_SESSION);
//melyik oldalon járunk, ha nem tudjuk 0 azaz dashboard
$page = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;
//baseURL kialakítása
$baseURL = APP_ADMIN_URL.'index.php?p='.$page;

if (filter_input(INPUT_GET, 'logout')) {//kilépés ha kell
    logout();
}
$auth = auth();//ellenőrzés
if (!$auth) {//ha nem jó az ellenőrzés
    header('location:login.php');
    exit();
}

//modul kezelés
    /** @var $adminMenu array a settingsből */
$modulFile = MODULE_DIR.$adminMenu[$page]['modulName'].MODULE_EXT;//module file név kialakítása
    //$output //valamelyik ágból lesz vagy modul vagy itt
    //ha létezik töltsük be
    if(file_exists($modulFile)){//itt keletkezik output
        include $modulFile;
    }else{
        $output = 'Nincs ilyen modul: '.$modulFile;
    }

//ha nincs érvényes bejelentkezés akkor irány a login
$userBar = '<section class="userbar">
                <span>Üdvözlet kedves <b>' . $_SESSION['userdata']['name'] . '</b>!</span>
                |
                <a href="?logout=true">kijelentkezés</a>
            </section>';
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ruander PHP tanfolyam - Admin felület</title>
</head>
<body>
<?php
echo $userBar;




?>
<header>
    <?php
    echo $menuHtml;
    ?>
</header>
<main>
    <?php
    //output kiírása
    echo $output;
    ?>
</main>
<footer>
    Ruander Oktatóközpont &copy; 2020 - <?php echo date('Y'); ?> - CMS minimal felület
</footer>
</body>
</html>
