<?php

/**
 * Beléptető eljárás
 * @return bool
 */
function login(): bool
{
    //lássuk a db linket
    global $link;
// email/jelszó páros ellenőrzése
    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');

//jelszóellenőrzés
    $qry = "SELECT id,name,password FROM users WHERE email = '$email' AND status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $rowUser = mysqli_fetch_assoc($result);
    //var_dump($rowUser);
    if ($rowUser && password_verify($password, $rowUser['password'])) {
        //echo 'jelszó oké';
        //mf elemek eltárolása
        //mf azonosító
        $sid = session_id();
        //mf jelszó az azonosításhoz
        $spass = md5($rowUser['id'] . $sid . SECRET_KEY);
        //időpont
        $now = $stime = time();//timestamp 1970.01.01 00:00:00 eltelt mp-ek száma
        //safety delete beragadt mf-okra
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' ") or die(mysqli_error($link));

        //tároljuk el a belépés adatait
        $qry = "INSERT INTO sessions(sid,spass,stime) VALUES('$sid','$spass',$stime)";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //tároljuk a $_SESSION ben a szükséges adatokat
        $_SESSION = [
            'id' => $sid,
            'userdata' => [
                'id' => $rowUser['id'],
                'name' => $rowUser['name'],
                'email' => $email
            ]
        ];
        return true;
    }
    return false;

}

/**
 * Belépés meglétének ellenőrzése authentication
 * @return bool
 */
function auth()
{
    global $link;
//érvényes belépés ellenőrzése
//van ilyen record?
//lejárt mf kezelése
    $now = time();
    $expired = $now - 15 * 60; //15 perc lejárat
//öntisztítás - összes lejárt mf törlése
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
    $qry = "SELECT spass FROM sessions WHERE sid = '" . session_id() . "' AND stime > $expired LIMIT 1";
//állítsuk össze a jelszót a rendelkezésre álló adatokból
    $spass = md5($_SESSION['userdata']['id'] . $_SESSION['id'] . SECRET_KEY);

    $result = mysqli_query($link, $qry) or die(myslqi_error($link));
    $row = mysqli_fetch_row($result);
    if ($row && $spass === $row[0]) {
        //stime update
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '{$_SESSION['id']}' LIMIT 1") or die(mysqli_error($link));

        return true;
    }
    return false;
}

/**
 * Kijelentkezés
 * @return void
 */
function logout()
{
    global $link;
    //db record törlése
    mysqli_query($link, "DELETE FROM sessions WHERE sid = '{$_SESSION['id']}' LIMIT 1") or die(mysqli_error($link));
    //mf tömb legyen üres
    $_SESSION = [];
    //mf roncsolás
    session_destroy();
}
/***DB segédfüggvények***/
/**
 * @param $dbTable
 * @param array $data
 * @return string
 */
function insertSQLQuery(string $dbTable, array $data = []): string
{
    $fields = implode('`,`',array_keys($data));//mezőnevek előkészítése
    $values = implode("','",$data);//értékek előkészítése
    $sql = "INSERT INTO $dbTable(`$fields`) VALUES('$values')";

    return $sql;
}

/**
 * @param $dbTable
 * @param $id
 * @param array $data
 * @return string
 */
function updateSQLQuery(string $dbTable, int $id, array $data = []): string
{
    $temp = [];
    foreach ($data as $k => $v){
        $temp[] = " `$k` = '$v' ";
    }

    $sql = "UPDATE {$dbTable} SET ".implode(',',$temp)." WHERE id = '$id' LIMIT 1";

    return $sql;
}

/**
 * Ékezettelenítő függvény filenevekhez
 * @param $str
 * @return string
 */
function formatFileName($str)
{
    $charsFrom = ['á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'Ä', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű', ' '];
    $charsTo = ['a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-'];
    $str = mb_strtolower($str, "utf-8");//kisbetűsre
    $str = str_replace($charsFrom, $charsTo, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str, '-');//végződő '-' eltávolítása

    return $str;
}

/**
 * szöveges tipusu input elemek beírt value értékét adja vissza
 * @param $fieldName | mező neve
 * @param $subKey | ha 2 dimenziós a post elem
 * @param $rowData | ha van az adott elemhez adatbázisból lekért adat
 * @return mixed
 */
function getValue($fieldName, $subKey = false, $rowData = [])
{

    if ($subKey) {//ha van 2. paraméter
        return filter_input(INPUT_POST, $fieldName, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY)[$subKey] ?? '';
    } elseif (filter_input(INPUT_POST, $fieldName)) {//ha van ilyen a postban
        return filter_input(INPUT_POST, $fieldName);
    } elseif (array_key_exists($fieldName, $rowData)) {//ha van az adatbázisból ilyen nevű adat
        return $rowData[$fieldName];
    }

}

/**
 * Input elemek hibaüzenetét adja vissza egy $errors[fieldName] szerkezetű hibatömbből
 * @param $fieldName
 * @return false|mixed
 */
function getError($fieldName, $subKey = false)
{
    global $errors;//az eljárás idejére globálissá tesszük az errors tömböt

    if ($subKey) {//ha van 2. paraméter

        if (isset($errors[$fieldName][$subKey])) {
            return $errors[$fieldName][$subKey];
        }
    } elseif (isset($errors[$fieldName])) {//ha nincs subkey

        return $errors[$fieldName];
    }

    return false;
}
