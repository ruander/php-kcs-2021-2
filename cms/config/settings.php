<?php
//kulcs mindenféle titkosításhoz
const SECRET_KEY = '_s3Cr3T_K3y!';
const MODULE_DIR = 'modules/';
const MODULE_EXT = '.php';
const APP_ADMIN_URL = 'http://admin.php-tanfolyam.local/';//itt működik az admin
const APP_URL = 'http://php-tanfolyam.local/'; //publikus url
//menü felépítése
$adminMenu = [
    0 => [
        'title' => 'Vezérlőpult',
        'modulName' => 'dashboard',
        'icon' => 'fas fa-tachometer-alt'
    ],
    1 => [
        'title' => 'Cikkek',
        'modulName' => 'articles',
        'icon' => 'fas fa-copy'
    ],
    12 => [
        'title' => 'Felhasználók',
        'modulName' => 'users',
        'icon' => 'fa fa-user'
    ],
];
