let pos;//itt lesz az oldal pozíció scrollY értéke
//fő fejléc
let mainHeader = document.querySelector('.main-header');
/*gördítés esemény*/
window.onscroll = function () {
    //console.log(window.scrollY);
    pos = window.scrollY;
    //ha elér egy bizonyos értéket, írjuk ki hogy sticky vagy nem sticky fejléc kell
    if (pos > 65) {
        //console.log('sticky', mainHeader);
        mainHeader.classList.add('sticky');//adjuk hozzá a sticky osztályt
    } else {
        //console.log('nem sticky')
        mainHeader.classList.remove('sticky');//vegyük el a sticky osztályt
    }
}

/*menü toggle működése*/
let menuToggle = document.getElementById('menu-toggle');
let mainMenu = document.querySelector('.main-header > nav > .main-menu');
//console.log(menuToggle, mainMenu);
menuToggle.onclick = function () {
    if(mainMenu.style.display === '' || mainMenu.style.display === 'none'){
        mainMenu.style.display = 'block';
    }else{
        mainMenu.style.display = 'none';
    }
    //console.log(mainMenu.style.display);
}
