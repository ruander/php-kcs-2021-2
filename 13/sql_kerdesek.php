<?php
require 'connect.php';
/** adatbázis csatlakozás betöltése @var $link mysqli */

//1.
$qry = "SELECT CONCAT(firstname,' ',lastname) alkalmazott FROM employees";//lekérés összeállítása

$result = mysqli_query($link, $qry) or die(mysqli_error($link));
var_dump($result);

//3. 	Melyik vevő rendelte eddig a legtöbbet (1:db -quantityordered vagy order darabszám, 2:érték)?
//a: order darabszám COUNT(ordernumber)
$qry = "SELECT 
            customers.customernumber,customername,COUNT(orderdate) db
        FROM customers
        RIGHT JOIN orders
        ON customers.customerNumber = orders.customerNumber
        GROUP BY customers.customerNumber
        ORDER BY db DESC
        LIMIT 1";//lekérés összeállítása

$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//most max 1 sor lehet benne ezért nem kell ciklus
$customer = mysqli_fetch_assoc($result);
echo '<pre>' . var_export($customer, true) . '</pre>';

$output = '<h3>3. a:(order darabszám) Melyik vevő rendelte eddig a legtöbbet: ' . $customer['customername'] . ' - <u>' . $customer['db'] . ' darab</u></h3>';

//válasz kiírása
echo $output;

//10. 	legelső megrendelés dátuma, ki rendelte?

$qry = "SELECT 
            customers.customerNumber,customerName,orderDate
        FROM customers
        RIGHT JOIN orders
        ON customers.customerNumber = orders.customerNumber
        ORDER BY orderdate
        LIMIT 1";//lekérés összeállítása

$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//most max 1 sor lehet benne ezért nem kell ciklus
$customer = mysqli_fetch_assoc($result);
echo '<pre>' . var_export($customer, true) . '</pre>';

$output = '<h3>10. A legelső megrendelés dátuma: ' . $customer['orderDate'] . ' - <u>' . $customer['customerName'] . '</u> ['.$customer['customerNumber'].']</h3>';

//válasz kiírása
echo $output;


//19. Alkalmazott teljes neve, főnöke teljes nev , ha nincs neki akkor 'Boss'
/*
SELECT
    CONCAT(e.firstname,' ',e.lastname) alkalmazott ,
    CONCAT(e2.firstname,' ',e2.lastname) fonok
FROM employees e
LEFT JOIN employees e2
ON
    e.reportsto = e2.employeenumber

----- 'BOSS'
SELECT
    CONCAT(e.firstname,' ',e.lastname) alkalmazott ,
    IF(
    e2.firstName IS NULL,
        'BOSS',
        CONCAT(e2.firstname,' ',e2.lastname)
    ) fonok
FROM employees e
LEFT JOIN employees e2
ON
    e.reportsto = e2.employeenumber;
 */
/**
 * @todo HF: minden válasz lehetőleg a txt fileból legyen itt is, de a lekérések mindenképp legyenek meg
 * @todo A register php file adatait milyen táblába tárolnád el (táblaterv-> mezők nevei, tipusai, beállítása)
 */
