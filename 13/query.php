<?php
require 'connect.php';/** adatbázis csatlakozás betöltése @var $link mysqli   */

$qry = "SELECT * FROM employees";//lekérés összeállítása

$result = mysqli_query($link,$qry) or die(mysqli_error($link));

//kapott eredmény feldolgozása
$row = mysqli_fetch_row($result);//egy sor kibontása tömbbe
echo '<pre>'.var_export($row,true).'</pre>';

$row = mysqli_fetch_assoc($result);//egy sor kibontása asszociatív tömbbe
echo '<pre>'.var_export($row,true).'</pre>';
echo $row['firstName'].' '.$row['lastName'];

$row = mysqli_fetch_array($result);//egy sor kibontása tömbbe
echo '<pre>'.var_export($row,true).'</pre>';
echo $row['firstName'].' '.$row[1];

$row = mysqli_fetch_object($result);//egy sor kibontása tömbbe
//echo '<pre>'.var_export($row['firstName'],true).'</pre>';//error
//echo '<pre>'.var_export($row->firstName,true).'</pre>';
echo '<pre>'.var_export($row,true).'</pre>';

//maradék kibontása egyszerre
$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);
echo '<pre>'.var_export($rows,true).'</pre>';

//írjuk listába a maradék 19 employee-t ($rows)
$employeesList = '<ul>';
//listaelemek
foreach($rows as $nr => $employee){
    $employeesList .="<li>". ++$nr .". {$employee['firstName']} {$employee['lastName']} </li>";
}

$employeesList .= '</ul>';
//kiírjuk
echo $employeesList;
