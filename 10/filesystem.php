<?php

$directories = scandir(__DIR__);

echo '<pre>'.var_export($directories, true).'</pre>';

//ellenőrzések
var_dump( is_file($directories[2]) );//file-e
var_dump( is_dir($directories[2]) );//mappa-e
var_dump( file_exists($directories[2]) );//létezik-e

//mappa létrehozása
$dir = 'testmappa/mappa2/';

//ha nem létezik a mappa, akkor hozzuk létre (rekurzív forma)
if( !is_dir($dir) ){
    mkdir($dir, true,  0755);
}

//mappa törlése - (csak üres mappa törölhető!)
rmdir('testmappa/mappa2/');
rmdir('testmappa/');

//file műveletek

//file méret
$fileName = 'test.txt';
if(file_exists($fileName)){
    echo '<br>'.filesize($fileName);

    //@todo: HF átnézni: fopen(),fread(),fwrite(),fclose https://www.php.net/manual/en/ref.filesystem.php
    //tegyünk tartalmat a test.txt fileba.
    file_put_contents('test.txt','Hello world!');

    clearstatcache();//a beolvasott és tárolt adatok (cache) törlése (filesize)
    echo '<br>'.filesize($fileName);

}else{
    echo '<br>Nincs ilyen file!';
}

//file készítés

$fileName = 'test2.txt';
$content = 'Ez egy teszt'.PHP_EOL.'szöveg...'.PHP_EOL;
file_put_contents($fileName,$content);//ha van felülír, ha nincs létrehoz
echo '<br>'.filesize($fileName);

//hozzáírás
var_dump(file_put_contents($fileName,$content,FILE_APPEND));


/*if( !file_exists('aaa.txt')){//ha nincs meg a file, die, vagy trigger_error
    die('Nincs meg a file!');
}*/

//filetartalom soronkénti beolvasása
$file = fopen($fileName,'r');

while(($line = fgets($file)) !== false){
    echo '<br>'.$line;
}

fclose($file);//file bezárása
//file másolása
copy($fileName, 'test3.txt');

rename('test3.txt','test4.txt');//átnevezés

unlink('test4.txt');

//filetartalom beolvasása
$fileContent = file_get_contents($fileName);
var_dump($fileContent);

//tartalom beolvasása urlről
$target = 'https://ruander.hu';
$pageContent = file_get_contents($target);
var_dump($pageContent);
