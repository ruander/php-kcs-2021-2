<?php

//data mappába tároljunk el egy adathalmazt
$dir = 'data/';
if(!is_dir($dir)){
    mkdir($dir, 0755, true);
}

$data = [
    'email' => 'hgy@iworkshop.hu',
    'name' => 'hgy',
    'password' => password_hash('123456',PASSWORD_BCRYPT),
    'time_created' => date('Y-m-d H:i:s')
];//adathalmaz

echo '<pre>'.var_export($data, true).'</pre>';

$fileName = 'user.txt';

file_put_contents($dir.$fileName , $data);//működik, automatikus array to string konverzió, viszont a tárolt formátum alkalmatlan az adatok beolvasás utáni értelmezésére

var_dump(file_get_contents($dir.$fileName));

//adatok átalakítása

//1. serialize
$serializedData = serialize($data);//sorozat (string)
file_put_contents($dir.$fileName, $serializedData);
$fileContent = file_get_contents($dir.$fileName);
var_dump($fileContent);
$dataFromSerialized = unserialize($fileContent);//visszaalakítás
var_dump($dataFromSerialized);

if($data === $dataFromSerialized){
    echo "<br>Az adatok tárolás után pontosan egyeznek (serialize)";
}else{
    echo "<br>Az adatok tárolás után NEM pontosan egyeznek (serialize)";
}


//2. CSV formátum
$fileName = 'user.csv';
//írjuk ki az adatokat (data/user.csv) CSV formátumban, ahol a határoló legyen ';'
$handle = fopen($dir.$fileName,'w');
fputcsv($handle, $data, ';');
fclose($handle);
//olvassuk be az adatokat a csv fileból
global $dataFromCSV;//itt tároljuk majd a kinyert adatokat
$row = 1;
if (($handle = fopen($dir.$fileName, "r")) !== FALSE) {
    while (($d = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $dataFromCSV = $d;
        $num = count($d);
        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            echo $d[$c] . "<br />\n";
        }
    }
    fclose($handle);

    //kinyert adatok
    echo '<pre>'.var_export($dataFromCSV, true).'</pre>';
}

//hasonlítsuk össze az eredetivel és írjuk ki hogy teljes mértékben egyeznek-e
//érték és tipus vizsgálat (nem egyezik)
if($data === $dataFromCSV){
    echo "<br>Az adatok tárolás után pontosan egyeznek (CSV)";
}else{
    echo "<br>Az adatok tárolás után NEM pontosan egyeznek (CSV)";
}
//csak érték vizsgálat (nem egyezik)
if($data == $dataFromCSV){
    echo "<br>Az adatok tárolás után értékben egyeznek (CSV)";
}else{
    echo "<br>Az adatok tárolás után értékben NEM egyeznek (CSV)";
}
//data átalakítás után (csak értékek)
if(array_values($data) === $dataFromCSV){
    echo "<br>Az adatok tárolás után értékben egyeznek (CSV)";
}else{
    echo "<br>Az adatok tárolás után értékben NEM egyeznek (CSV)";
}
//fgetcsv,fputcsv
//https://www.php.net/manual/en/function.fputcsv.php

//3 json formátum
$fileName = 'user.json';
$jsonData = json_encode($data);//json (string)
file_put_contents($dir.$fileName, $jsonData);
$fileContent = file_get_contents($dir.$fileName);
var_dump($fileContent);
$dataFromJSon = json_decode($fileContent, true);//visszaalakítás
var_dump($dataFromJSon);

if($data === $dataFromJSon){
    echo "<br>Az adatok tárolás után pontosan egyeznek (json)";
}else{
    echo "<br>Az adatok tárolás után NEM pontosan egyeznek (json)";
}

/*
 * @todo HF 2 a 9es mappa registration.php fileban hibátlan űrlap esetén kapott adattömböt tárold el json formátumban egy 'data/user.json' nevű fileban, ha van kedved akkor a tippeket is 'data/tipp.json' nevű fileba
 */
