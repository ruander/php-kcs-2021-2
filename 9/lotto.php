<?php
require "functions.php";

/*ha vannak tippjeink (post) akkor adatfeldolfozás
    hibakezelés (hibaüzenetek kialakítása)
    ha nincs hiba - művelet
        tippek eltárolása fileban
űrlap a tippeknek (ciklusban) - hibaüzenetekkel ha vannak*/
/*Erőforrások*/
$limit = 90;//1 és ennyi között lesznek az érvényes értékek
$fields = 5;//ennyi tippmező kell
if($fields>$limit){//ha rossz erősorrás paraméterezést kapunk
    trigger_error("Gáz van! Érvénytelen erőforrás értékek!", E_USER_ERROR);
}
if(!empty($_POST)){
    //echo '<pre>'.var_export($_POST, true).'</pre>';

    $errors = [];
    //név min 3 karakter
    $name = trim(filter_input(INPUT_POST, 'name'));
    $name = strip_tags($name); //tagek eltávolítása

    if( mb_strlen($name,"utf-8") < 3 ){
        $errors['name'] = '<span class="error">Minimum 3 karakter!</span>';
    }

    //email
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);

    if(!$email){
        $errors['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    //tippek
    $tippek = filter_input(INPUT_POST,'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    //echo '<pre>'.var_export($tippek, true).'</pre>';
    //egyedi tippek segédtömb
    $eggyedi_tippek = array_unique($tippek);
    //echo '<pre>'.var_export($eggyedi_tippek, true).'</pre>';

    //bejárjuk a tippeket és hibakezeljük őket
    foreach ($tippek as $id => $tipp){
        //1-limit
        if($tipp < 1 || $tipp > $limit){
            $errors['tippek'][$id] = '<span class="error">Nem érvényes formátum!</span>';
        }elseif( !array_key_exists($id,$eggyedi_tippek) ){//imsétlődő tippek hibaüzenete
            $errors['tippek'][$id] = '<span class="error">Már tippelted!</span>';
        }
    }
    //echo '<pre>'.var_export($errors, true).'</pre>';

    if(empty($errors)){//adatok tisztázása
        $data = [
            'name' => $name,
            'email' =>$email,
            'time_created' => date('Y-m-d H:i:s'),
            'tippek' => $tippek
        ];
        echo '<pre>'.var_export($data, true).'</pre>';
    }

}


//űrlap összeállítása
$form = '<form method="post">';
//Név
$form .= '<label>
                <span>Név<sup>*</sup></span>
                <input type="text" name="name" placeholder="Gipsz Jakab" value="' . getValue('name') . '">';

$form .= getError('name');//hiba ha van, belefűzzük
$form .= '</label>';
//PURE PHP űrlap elemek
//email
$form .= '<label>
                <span>Email<sup>*</sup></span>
                <input type="text" name="email" placeholder="email@cim.hu" value="' . getValue('email') . '">';
$form .= getError('email');
$form .= '</label>';

//ciklus a tippeknek
for($i=1;$i<=$fields;$i++){
    $form .= '<label>
                <span>Tipp '.$i.'<sup>*</sup></span>
                <input type="text" name="tippek['.$i.']" placeholder="'.$i.'" value="' . getValue('tippek', $i) . '">';
    //echo filter_input(INPUT_POST,'tippek',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY)[$i]??'';//$tippek[$i] //operátor ?? -> isset($v) ? $v : '' ==> $v ?? ''
    /*if(isset($errors['tippek'][$i])){
        $form .= $errors['tippek'][$i];
    }*/

    $form .= getError('tippek',$i);
    //$form .= getError('tipp'.$i);
    $form .= '</label>';
}



//küldés gomb és form zárás
$form .= '<button>Tippek beküldése</button></form>';

echo $form;

//stílusok, ideiglenesen
$styles = '<style>
        label {
            display: flex;
            flex-direction: column;
            margin: 10px;
        }

        label.inline {
            display: block;
        }

        .error {
            font-size: .8em;
            font-style: italic;
            color: #f00;
        }
    </style>';
echo $styles;
