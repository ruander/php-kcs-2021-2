<?php
echo '<pre>' . var_export($_POST, true) . '</pre>';

if(!empty($_POST)){
    $errors = [];

    $education = filter_input(INPUT_POST, 'education');

    if(!$education){
        $errors['education'] =  '<span class="error">Kötelező választani!</span>';
    }

}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Select kiválasztott adat megtartása és (required)</title>
</head>
<body>
<form method="post">
    <label>
        Legmagasabb iskolai végzettség <sup>*</sup>

        <?php
        $form = '';
        $form .= '<select name="education">
                    <option value="">--válassz--</option>';
        //opciók
        $educations = [
            'basic' => 'Általános 8 osztály',
            'middle' => 'Középiskola',
            'high_school' => 'Gimnázium',
            'university' => 'Egyetem'
        ];
        foreach($educations as $optionValue => $optionText){
            $selected = filter_input(INPUT_POST,'education') === $optionValue ? 'selected' : '';
            $form .= '<option value="' . $optionValue . '" '.$selected.'>' . $optionText. '</option>';
        }
        $form .= '</select>';
        //hiba ha van
        if(isset($errors['education'])){
            $form .= $errors['education'];
        }
        echo $form;

        ?>

    </label>
    <button>mehet</button>
</form>
</body>
</html>
