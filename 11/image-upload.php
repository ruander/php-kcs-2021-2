<?php
//erőforrás:
//elfogadott filetipusok
const AVIABLE_IMAGE_TYPES = ['image/jpeg', 'image/jpg'];

//echo '<pre>' . var_export($_POST, true) . '</pre>';

if (!empty($_POST)) {
    $errors = [];
    //echo '<pre>' . var_export($_FILES, true) . '</pre>';

    //ha nincs feltöltött file akkor hiba (kötelező mező)
    if ($_FILES['img_to_upload']['error'] !== 0) {
        $errors['img_to_upload'] = '<span class="error">Kötelező mező!</span>';
    } elseif (!in_array($_FILES['img_to_upload']['type'], AVIABLE_IMAGE_TYPES)) {
        $errors['img_to_upload'] = '<span class="error">Nem engedélyezett filetipus!</span>';
    }

    //képtipus ellenőrzése MIME
    $imageInfo = getimagesize($_FILES['img_to_upload']['tmp_name']);
    if (!$imageInfo) {
        $errors['img_to_upload'] = '<span class="error">Nem engedélyezett képtipus!</span>';
    }

    //ha van feltöltött file akkor dolgozzuk fel

    if (empty($errors)) {
        //van feltöltött file, különben hiba lenne
        $dir = 'images/';
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        //képátalakítás méretarányosan, úgy hogy a nagyobb méretet maximáljuk pl 600px : fekvő esetén max 600px széles, arányos kép, fekvő esetben 600px magas arányos kép
        //$targetWidth, $targetWidth
        $originalWidth = $imageInfo[0];
        $originalHeight = $imageInfo[1];
        //képarány
        $ratio = $originalWidth / $originalHeight;
        //koordináta rendszer adatok
        $src_x = $src_y = 0;
        $target_x = $target_y = 0;
        //álló és fekvő esetek szétválasztása
        if ($ratio > 1) {
            //fekvő
            $targetWidth = 600;
            $targetHeight = round($targetWidth / $ratio);
        } else {
            //álló vagy négyzet
            $targetHeight = 600;
            $targetWidth = round($targetHeight * $ratio);
        }

        //képműveletek
        $canvas = imagecreatetruecolor($targetWidth, $targetHeight);
        //eredeti kép memóriába (tipusfüggő)
        $src_image = imagecreatefromjpeg($_FILES['img_to_upload']['tmp_name']);


        //imagecopyresampled(
        //    GdImage $dst_image,
        //    GdImage $src_image,
        //    int $dst_x,
        //    int $dst_y,
        //    int $src_x,
        //    int $src_y,
        //    int $dst_width,
        //    int $dst_height,
        //    int $src_width,
        //    int $src_height
        //)
        imagecopyresampled($canvas, $src_image, $target_x, $target_y, $src_x, $src_y, $targetWidth, $targetHeight, $originalWidth, $originalHeight);

        //kép mutatása memóriából, azaz ez a a file egy képnek fog látszani. A művelet előtt semmi nem lehet kiírva
        /*header('Content-type: image/jpeg');
        imagejpeg($canvas, null, 100);*/

        //kiírás lemezre eredeti néven
        $fileName = formatFileName($_FILES['img_to_upload']['name']);

        //@todo 2: HF - A LOTTO szelvények legyen egy fileban tárolva és legyen egy szelvenyek.php file ami egy listában mutatja ki milyen emaillel milyen tippeket adott fel és mikor
        imagejpeg($canvas, $dir.'600-'.$fileName , 100);

        //takarítás - memória felszabadítása
        imagedestroy($canvas);
        imagedestroy($src_image);
        echo '<pre>';
        var_dump(is_file($_FILES['img_to_upload']['tmp_name']));
        echo '</pre>';





    }

}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kép feltöltés</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <label>
        Tölts fel egy képet<sup>*</sup>
        <?php
        $form = '';
        $form .= '<input type="file" name="img_to_upload">';
        //hiba ha van
        if (isset($errors['img_to_upload'])) {
            $form .= $errors['img_to_upload'];
        }
        echo $form;

        ?>

    </label>
    <button name="submit">mehet</button>
</form>
</body>
</html><?php

/**
 * @param $str
 * @return string
 */
function formatFileName($str)
{
    $charsFrom = ['á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'Ä', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű', ' '];
    $charsTo = ['a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-'];
    $str = mb_strtolower($str, "utf-8");//kisbetűsre
//    echo "<br>$str";
    $str = str_replace($charsFrom, $charsTo, $str);//karakterek cseréje
//    echo "<br>$str";
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
//    echo "<br>$str";
    $str = rtrim($str, '-');//végződő '-' eltávolítása
    return $str;
}
