<?php
//adatok beolvasása fileból
$dir = "users/";
$fileName = 'users.json';

//ha létezik a file, olvassuk be egy változóba a tartalmát
if(file_exists($dir.$fileName)) {
    $fileContent = file_get_contents($dir . $fileName);

    $user = json_decode($fileContent, true);

    echo '<pre>' . var_export($user, true) . '</pre>';
}else{
    echo 'Nincs ilyen file: '.$dir.$fileName;
}
