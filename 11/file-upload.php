<?php
echo '<pre>' . var_export($_POST, true) . '</pre>';

if(!empty($_POST)){
    $errors = [];
    echo '<pre>' . var_export($_FILES, true) . '</pre>';

    //ha nincs feltöltött file akkor hiba (kötelező mező)
    if($_FILES['file_to_upload']['error'] !== 0){
        $errors['file_to_upload'] = '<span class="error">Kötelező mező!</span>';
    }

    //ha van feltöltött file akkor dolgozzuk fel

    if(empty($errors)){
        //van feltöltött file, különben hiba lenne
        $dir = 'uploads/';
        if(!is_dir($dir)){
            mkdir($dir,0755,true);
        }
        //filenév kialakítása lara-web-szerzodes-alairasra-converted.docx
        $fileName = formatFileName( $_FILES['file_to_upload']['name'] );

        if(is_uploaded_file($_FILES['file_to_upload']['tmp_name'])){
            move_uploaded_file($_FILES['file_to_upload']['tmp_name'],$dir.$fileName);
        }

    }

}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>File feltöltés</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <label>
        Tölts fel egy filet<sup>*</sup>
        <?php
        $form = '';
        $form .= '<input type="file" name="file_to_upload">';
        //hiba ha van
        if(isset($errors['file_to_upload'])){
            $form .= $errors['file_to_upload'];
        }
        echo $form;

        ?>

    </label>
    <button name="submit">mehet</button>
</form>
</body>
</html><?php

/**
 * @param $str
 * @return string
 */
function formatFileName($str) {
    $charsFrom  = ['á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' '];
    $charsTo    = ['a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-'];
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    echo "<br>$str";
    $str = str_replace($charsFrom, $charsTo, $str);//karakterek cseréje
    echo "<br>$str";
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    echo "<br>$str";
    $str = rtrim($str,'-');//végződő '-' eltávolítása
    return $str;
}
