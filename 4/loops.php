<?php
//ciklusok
/*
 * elöltesztelő ciklus
for( ciklusváltozó kezdeti megadása; belépési feltétel vizsgálat ; ciklusváltozó léptetése ){
    //ciklusmag ...kód
}
 */
//írjuk ki a számokat 1-10ig
for( $i = 1 ; $i<=10 ; $i++){// operátor: $i = $i + 1 -> $i++, analógia: $i-- ($i**n)
    //ciklusmag
    echo "<br>$i";
}

//while
/*
* elöltesztelő változat
while(belépési feltétel vizsgálat){
    //ciklusmag
}
 */
$i = 1;//'ciklusváltozó'
while($i<=10){
    //ciklusmag
    echo "<br>".$i++;
    //$i++;//'ciklusváltozó' léptetése
}

//$i értéke itt:11
//var_dump($i);

/*
do - while (hátultesztelő)
do{
    //ciklusmag
}while(belépési feltétel vizsgálat)
 */
$i = 1;
do{
    //ciklusmag
    echo "<br>".$i++;
}while($i<=10);


$users = ['Tercsi','Fercsi','Kata','Klára'];
var_dump($users);
//írjuk ki az összes felhasználó nevét egy listába
/*
 Tömb bejárása

foreach(tömb|objektum as key => value){
    //ciklusmag
    key és value => aktuális elem
}
 */
foreach($users as $k => $v){
    echo "<br>Kulcs: $k , értéke: $v";
}
//listában csak a nevek

echo '<ul>';
foreach($users as $v){//most csak a value kell
    echo "<li>$v</li>";
}
echo '</ul>';


//optimálisabb ha nem echozgatunk mindenfelé, hanem 1 változóba gyüjtjük a kiírandó stringeket (fűzés) majd 1 lépésben kiírjuk ahol kell
//listában csak a nevek

$output = '<ul>';
foreach($users as $v){//most csak a value kell
    $output .= "<li>$v</li>";
}
$output .= '</ul>';

echo $output;

//töltsünk fel egy 5 elemű tömböt véletlen 1 és 100 közötti számokkal ()
//állítsuk növekvő sorrenbe
//írjuk ki sorszámmal a tömb elemeit:(foreach)
/*
 1. 13
 2. 23
 3. 42
...
 */
$numbers = [];
for( $i=1 ; $i<=5 ; $i++  ){
    $numbers[] = rand(1,100);
}
echo '<pre>'.var_export($numbers,true).' </pre>';
$numbers = [];
$i=1 ;
while(  $i<=5  ){
    $numbers[] = rand(1,100);
    $i++;
}
echo '<pre>'.var_export($numbers,true).' </pre>';

//count(tömb) tömb elemszáma
$numbers = [];
while(count($numbers) < 5){
    $numbers[] = rand(1,100);
}
echo '<pre>'.var_export($numbers,true).' </pre>';
//ugyanaz CSÚNYA for megoldással
for( ; count($numbers) < 5 ;  ){
    $numbers[] = rand(1,100);
}
echo '<pre>'.var_export($numbers,true).' </pre>';
