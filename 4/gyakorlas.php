<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.

for( $i=1 ; $i<=4 ; $i++){
    echo 'menü';
}


$menu = ['Home','About','Services','Contact'];
$output = '<nav><ul>';
//menüpontok
foreach ($menu as $k => $menuItem){
    $output .= '<li><a href="?link='.$k.'">'.$menuItem.'</a></li>';
}

$output .= '</ul></nav>';

echo $output;

//több dimenziós tömb és asszociatív kulcsok

$menu = [
    1 => [
        'title' => 'Home',
        'icon' => 'fa fa-home',
        'link' => 'home'
    ],
    5 => [
        'title' => 'About',
        'icon' => 'fa fa-user',
        'link' => 'about'
    ],
    7 => [
        'title' => 'Contact',
        'icon' => 'fa fa-envelope',
        'link' => 'contact'
    ],
];
echo '<pre>'.var_export($menu,true).'</pre>';
//többdimenziós tömb elemei
$output = '<nav><ul>';
//menüpontok
foreach ($menu as $menuId => $menuItem){
    $output .= '<li class="'.$menuItem['icon'].'"><a href="#'.$menuItem['link'].'">'.$menuItem['title'].'</a></li>';
}

$output .= '</ul></nav>';

echo $output;

/*
2. Készítsünk programot, amely kiszámolja az első 100 darab. természetes
szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához
vezessünk be egy változót, amelyet a program elején kinullázunk, a
ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát
sorban az 1, 2, 3, 4, ..., 100 számokat.)
 */
$sum = 0;
for($i=1;$i<=100;$i++){
    $sum += $i; //operátor: $sum = $sum + $i; -> $sum += $i;
}
echo $sum;
//3.
$mtpl = 1;
for($i=1;$i<=7;$i++){
    $mtpl *= $i; //operátor: $mtpl = $mtpl * $i; -> $mtpl *= $i;
}
echo '<br>'.$mtpl;
/**
 * @todo: HF alap feladatok összes (14-24), feladatgyüjtemény 4,5
 */
