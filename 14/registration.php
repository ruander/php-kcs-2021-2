<?php
//külső forrás betöltése mintha ide lenne gépelve
require "config/functions.php";//betöltjük a saját eljárásainkat
require "config/connect.php";/** @var $link mysqli */
//lokális erőforrások
$dbTable = 'users';

//ha van mit feldolgozni akkor feldolgozzuk
if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];// itt tároljuk a hibákat ha vannak
    //Név min 3 karakter
    $name = strip_tags(trim(filter_input(INPUT_POST, "name")));

    if (mb_strlen($name, "utf-8") < 3) {
        $errors['name'] = '<span class="error">Nem érvényes formátum! (minimum 3 karakter)</span>';
    }

    //email, legyen emailnek látszó string
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $errors["email"] = '<span class="error">Nem érvényes formátum!</span>';
    }else{//formátumra jó, foglalt-e?
        $result = mysqli_query($link, "SELECT id FROM $dbTable WHERE email = '$email' LIMIT 1") or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //ha van ilyen akkor már foglalt
        if(!empty($row)){
            $errors["email"] = '<span class="error">Már foglalt email cím!</span>';
        }
    }

    //jelszó 1 min 6 karakter
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password) < 6) {
        $errors["password"] = '<span class="error">Nem érvényes formátum! (minimum 6 karakter)</span>';
    } elseif ($password !== $repassword) {
        //jelszó 2 egyeznie kell a jelszó 1-el
        $errors["repassword"] = '<span class="error">A jelszavak nem egyeztek!</span>';
    } else {
        //jelszó kódolása tároláshoz
        $password = password_hash($password, PASSWORD_BCRYPT);
    }
    //státusz legyen most fix 1
    $status = 1;

    //Elfogadás checkbox legyen kipipálva
    if (!filter_input(INPUT_POST, 'terms')) {
        $errors["terms"] = '<span class="error">Kötelező kipipálni!</span>';
    }


    if (empty($errors)) {
        //nincs hiba az űrlapon, tegyünk rendet az adataink között
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];
        //mikor történt a regisztráció (time_created), tegyük a tömbünkbe az adatokhoz
        $data['time_created'] = date('Y-m-d H:i:s');

        //tároljuk el adabázisban
        $qry = "INSERT INTO 
            $dbTable (
                   name, 
                   email, 
                   password, 
                   status, 
                   time_created
                   ) 
            VALUES (
                    '{$data['name']}', 
                    '{$data['email']}', 
                    '{$data['password']}', 
                    '{$data['status']}', 
                    '{$data['time_created']}'
                    )";
        mysqli_query($link,$qry) or die(mysqli_error($link));

        //echo '<pre>' . var_export($_SERVER, true) . '</pre>';
        //file átirányítása az üres form-ra (saját magára)
        header('location:'.$_SERVER['PHP_SELF']);
        exit();
    }

}

//űrlap összeállítása
$form = '<form method="post">';
//Név
$form .= '<label>
                <span>Név<sup>*</sup></span>
                <input type="text" name="name" placeholder="Gipsz Jakab" value="' . getValue('name') . '">';

$form .= getError('name');//hiba ha van, belefűzzük
$form .= '</label>';
//PURE PHP űrlap elemek
//email
$form .= '<label>
                <span>Email<sup>*</sup></span>
                <input type="text" name="email" placeholder="email@cim.hu" value="' . getValue('email') . '">';
$form .= getError('email');
$form .= '</label>';
//jelszó1
$form .= '<label>
                <span>Jelszó<sup>*</sup></span>
                <input type="password" name="password" placeholder="******" value="">';
$form .= getError('password');
$form .= '</label>';

//jelszó2
$form .= '<label>
                <span>Jelszó mégegyszer<sup>*</sup></span>
                <input type="password" name="repassword" placeholder="******" value="">';
$form .= getError('repassword');
$form .= '</label>';
//terms
$form .= '<label class="inline">
                <input type="checkbox" name="terms" value="1"' . (getValue('terms') ? 'checked' : '') . '>';
$form .= getError('terms') . ' Elolvastam és megértettem az adatkezelési tájékoztatót.';
$form .= '</label>';

//küldés gomb és form zárás
$form .= '<button>Regisztrálok</button></form>';

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
            margin: 10px;
        }

        label.inline {
            display: block;
        }

        .error {
            font-size: .8em;
            font-style: italic;
            color: #f00;
        }
    </style>
</head>
<body>
<section class="registration">
    <?php

    //kiírás 1 lépésben
    echo $form;

    ?>
</section>
</body>
</html>
