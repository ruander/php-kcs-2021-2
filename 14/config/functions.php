<?php

/**
 * Ékezettelenítő függvény filenevekhez
 * @param $str
 * @return string
 */
function formatFileName($str) {
    $charsFrom  = ['á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' '];
    $charsTo    = ['a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-'];
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($charsFrom, $charsTo, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő '-' eltávolítása

    return $str;
}

/**
 * szöveges tipusu input elemek beírt value értékét adja vissza
 * @param $fieldName  | mező neve
 * @param $subKey | ha 2 dimenziós a post elem
 * @param $rowData | ha van az adott elemhez adatbázisból lekért adat
 * @return mixed
 */
function getValue($fieldName, $subKey = false, $rowData = []){

    if($subKey){//ha van 2. paraméter
        return filter_input(INPUT_POST,$fieldName,FILTER_DEFAULT,FILTER_REQUIRE_ARRAY)[$subKey]??'';
    }elseif(filter_input(INPUT_POST, $fieldName)){//ha van ilyen a postban
        return filter_input(INPUT_POST, $fieldName);
    }elseif(array_key_exists($fieldName,$rowData)){//ha van az adatbázisból ilyen nevű adat
        return $rowData[$fieldName];
    }

}

/**
 * Input elemek hibaüzenetét adja vissza egy $errors[fieldName] szerkezetű hibatömbből
 * @param $fieldName
 * @return false|mixed
 */
function getError($fieldName, $subKey = false){
    global $errors;//az eljárás idejére globálissá tesszük az errors tömböt

    if($subKey){//ha van 2. paraméter

        if( isset($errors[$fieldName][$subKey]) ){
            return $errors[$fieldName][$subKey];
        }
    }elseif( isset($errors[$fieldName]) ){//ha nincs subkey

        return $errors[$fieldName];
    }

    return false;
}
