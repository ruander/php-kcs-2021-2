<!doctype html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <title>PHP gyakorlás</title>
</head>
<body>
<?php
echo '<h2>18. Írjon egy programot, amely kiszámolja a körök területét, amikor a körök sugara az 1-től 10-ig terjedő egész számok, de csak akkor írja ki az értékeket, ha a terület nagyobb, mint 20.</h2>';

for($r=1;$r<=10;$r++){
   $area = $r**2 * M_PI;
   if($area > 20){
       echo "<br>$area m<sup>2</sup>";
   }
}

echo '<h2>22.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja, hogy a tömbben hány darab olyan szám van, amely nagyobb, mint 10 és kisebb 20.</h2>';
$test = [];
//feltöltés 1-30 közötti véletlen számokkal, 10 elem

while( count($test) < 10 ){
    $test[] = rand(1,30);
}
echo '<pre>'.var_export($test,true).'</pre>';
$sum = 0;
//bejárjuk az elkészült tömböt és megnézzük hány elemet találunk , ami a feltételeknek megfelel
foreach($test as $v){
        if($v > 10 AND $v < 20){
            $sum++;
        }
}

echo "A feltételeknek $sum db elem felet meg.";

//ha kellenek a szóban forgó elemek is.
$result = [];
foreach($test as $v){
    if($v > 10 AND $v < 20){
        $result[]=$v;
    }
}
echo '<pre>'.var_export($result,true).'</pre>';
echo 'A feltételeknek '.count($result).' db elem felelt meg: ' . implode(',', $result);

echo '<br>';
/*echo gettype($sum);//settype(változó, tipus)
$test =  "helo";
settype($test,'array');
echo '<br>';
echo gettype($test);//settype(változó, tipus)
var_dump($test);*/

?>
</body>
</html>
