<?php
//Karakterlap készítése

/*
 * a pontszám egy 4x 6-oldalú kockával dobásból a legjobb 3 összértéke
 */
function dice(){
    $result = [];
    while (count($result) < 4) {
        $result[] = rand(1, 6);
    }
    sort($result);//emelkedő érték sorrendbe rendezés
    //első elem eltávolítása
    unset($result[0]);
    //Visszatérünk a megmaradt tömb értékek összegével
    return array_sum($result);
}
//echo dice();

//echo '<pre>'.var_export($result,true).'</pre>';
//Erő, (3-18) ha nem tudod kiszedni a legkisebbet: 4-24
$output = '<ul>';

$output .= '<li>Erő: '. dice() .'</li>';
//Ügyesség,
$output .= '<li>Ügyesség: '. dice() .'</li>';

//Állóképesség,
$output .= '<li>Állóképesség: '. dice() .'</li>';

//Intelligencia,
$output .= '<li>Intelligencia: '. dice() .'</li>';

//Bölcsesség,
$output .= '<li>Bölcsesség: '. dice() .'</li>';

//Karizma
$output .= '<li>Karizma: '. dice() .'</li>';

$output .= '</ul>';

//Kiírjuk a tulajdonságok nevét és értékét a felhasználónak +
echo $output;

//picit másképp
$abilites = ['Erő','Ügyesség','Állóképesség','Intelligencia','Bölcsesség','Karizma'];
//képességek generálása ciklusban +output kialakítása
$output='<ul>';
//listaelemek elkészítése a tömb bejárásával és a dice() eljárás használatával
foreach($abilites as $ability_name){
    $output .= '<li>'.$ability_name.' | '. dice() .'</li>';
}
$output .= '</ul>';
echo $output;
// 1 link hogy UJ karakterlap generálása 'sheetgenerator.php'

//Kell html körítés is és a karakterlap tulajdonságok a bodyban jelenjenek meg
?>
<a href="sheetgenerator.php">Újra</a>
