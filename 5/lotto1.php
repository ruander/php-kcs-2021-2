<?php
//szimuláljuk az 5/90 lottóhúzást
//legyen üres tömbünk ahol a számokat eltároljuk
$result = [];
//ciklus 5x
while(count($result) < 5){
    //generáljunk 1 számot 1-90 között és tároljuk el a tömbben,ha még nem szerepel benne
    $d = rand(1,90);
    if( in_array($d,$result) === false ){
        $result[] = $d;
    }
}
//emelkedő értéksorrend
sort($result);
//tömb értékeinek kiírása
echo implode(',',$result);

/**
 * Lottószám generálása 1 és 90 közötti egyedi értéket tartalmazó rendezett tömbbe
 * @return array
 */
function getNumbers(){
    $result = [];
    while(count($result) < 5){
        $result[] = rand(1,90);
        //irtsuk ki az ismétlődéseket
        $result = array_unique($result);
    }
//emelkedő értéksorrend
    sort($result);

    return $result;
}
$result = getNumbers();
//tömb értékeinek kiírása
echo '<br>'.implode(',',$result);
